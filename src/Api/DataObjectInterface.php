<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\DataObject\Api;

/**
 * Interface DataObjectInterface.
 *
 * @api
 */
interface DataObjectInterface extends \ArrayAccess
{
    /**
     * Set/Get attribute wrapper (magic methods).
     *
     * @param string $method
     * @param array  $args
     *
     * @throws \RuntimeException
     *
     * @return mixed
     */
    public function __call(
        string $method,
        array $args
    );

    /**
     * Add data to the object.
     * Retains previous data in the object.
     *
     * @param array $data
     */
    public function addData(array $data): void;

    /**
     * Overwrite data in the object.
     *
     * The $key parameter can be string or array.
     * If $key is string, the attribute value will be overwritten by $value
     * If $key is an array, it will overwrite ALL the data in the object.
     *
     * @param string|array $key
     * @param mixed        $value
     */
    public function setData($key, $value = null): void;

    /**
     * Unset data from the object.
     *
     * @param string|array|null $key
     */
    public function unsetData($key = null): void;

    /**
     * Object data getter.
     *
     * If $key is not defined will return all the data as an array.
     * Otherwise it will return value of the element specified by $key.
     * It is possible to use keys like a/b/c for access nested array data
     *
     * If $index is specified it will assume that attribute data is an array
     * and retrieve corresponding member. If data is the string - it will be explode
     * by new line character and converted to array.
     *
     * @param string     $key
     * @param string|int $index
     *
     * @return mixed
     */
    public function getData(string $key = '', $index = null);

    /**
     * Get object data by path
     * Method consider the path as chain of keys: a/b/c => ['a']['b']['c'].
     *
     * @param string $path
     *
     * @return mixed
     */
    public function getDataByPath(string $path);

    /**
     * Get object data by particular key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getDataByKey(string $key);

    /**
     * Set object data with calling setter method.
     *
     * @param string $key
     * @param mixed  $args
     */
    public function setDataUsingMethod(string $key, $args = []): void;

    /**
     * Get object data by key with calling getter method.
     *
     * @param string $key
     * @param mixed  $args
     *
     * @return mixed
     */
    public function getDataUsingMethod($key, $args = null);

    /**
     * If $key is empty, checks whether there's any data in the object
     * Otherwise checks if the specified attribute is set.
     *
     * @param string $key
     *
     * @return bool
     */
    public function hasData($key = ''): bool;

    /**
     * Convert array of object data with to array with keys requested in $keys array.
     *
     * @param array $keys array of required keys
     *
     * @return array
     */
    public function toArray(array $keys = []): array;

    /**
     * The "__" style wrapper for toArray method.
     *
     * @param array $keys
     *
     * @return array
     */
    public function convertToArray(array $keys = []): array;

    /**
     * Convert object data into XML string.
     *
     * @param array  $keys       array of keys that must be represented
     * @param string $rootName   root node name
     * @param bool   $addOpenTag flag that allow to add initial xml node
     * @param bool   $addCdata   flag that require wrap all values in CDATA
     *
     * @return string
     */
    public function toXml(
        array $keys = [],
        string $rootName = 'item',
        bool $addOpenTag = false,
        bool $addCdata = true
    ): string;

    /**
     * The "__" style wrapper for toXml method.
     *
     * @param array  $keys       array of keys that must be represented
     * @param string $rootName   root node name
     * @param bool   $addOpenTag flag that allow to add initial xml node
     * @param bool   $addCdata   flag that require wrap all values in CDATA
     *
     * @return string
     */
    public function convertToXml(
        array $keys = [],
        string $rootName = 'item',
        bool $addOpenTag = false,
        bool $addCdata = true
    ): string;

    /**
     * Convert object data to JSON.
     *
     * @param array $keys array of required keys
     *
     * @throws \InvalidArgumentException
     *
     * @return bool|string
     */
    public function toJson(array $keys = []);

    /**
     * The "__" style wrapper for toJson.
     *
     * @param array $keys
     *
     * @throws \InvalidArgumentException
     *
     * @return bool|string
     */
    public function convertToJson(array $keys = []);

    /**
     * Convert object data into string with predefined format.
     *
     * Will use $format as an template and substitute {{key}} for attributes
     *
     * @param string $format
     *
     * @return string
     */
    public function toString($format = ''): string;

    /**
     * Checks whether the object is empty.
     *
     * @return bool
     */
    public function isEmpty(): bool;

    /**
     * Convert object data into string with defined keys and values.
     *
     * Example: key1="value1" key2="value2" ...
     *
     * @param array  $keys           array of accepted keys
     * @param string $valueSeparator separator between key and value
     * @param string $fieldSeparator separator between key/value pairs
     * @param string $quote          quote character
     *
     * @return string
     */
    public function serialize(
        array $keys = [],
        string $valueSeparator = '=',
        string $fieldSeparator = ' ',
        string $quote = '"'
    ): string;

    /**
     * Present object data as string in debug mode.
     *
     * @param mixed $data
     * @param array &$objects
     *
     * @return array
     */
    public function debug(
        $data = null,
        array &$objects = []
    ): array;
}
