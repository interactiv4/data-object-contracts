<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\DataObject\Api;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Interface DataObjectInterfaceFactoryInterface.
 *
 * @api
 */
interface DataObjectInterfaceFactoryInterface extends FactoryInterface
{
    /**
     * {@inheritdoc}
     *
     * @return DataObjectInterface
     */
    public function create(array $arguments = []): DataObjectInterface;
}
